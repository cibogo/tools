#!/bin/bash

# Enter the username
username=bsm$(shuf -i 19999-99999 -n 1)
while [[ $username = "" ]]; do
    echo "Enter the proxy username"
    read -p "username: " username
    if [ -z "$username" ]; then
      echo "The username cannot be empty"
    else
        # Check if user already exists.
        grep -wq "$username" /etc/passwd
        if [ $? -eq 0 ]
            then
            echo "User $username already exists"
            username=
        fi
    fi
done

# Enter the proxy user password
password=bsm$(shuf -i 19999-99999 -n 1)
while [[ $password = "" ]]; do
    echo "Enter the proxy password"
    read -p "password: " password
    if [ -z "$password" ]; then
        echo "Password cannot be empty"
    fi
done

# Install  dante-server
apt-get update -y > /dev/null 2>&1
apt-get install wget dante-server iproute2 -y

# determine default int
#default_int="$(ip route list |grep default |grep -o -P '\b[a-z]+\d+\b')" #Because net-tools in debian, ubuntu are obsolete already
# determine external ip
external_ip="$(wget ipinfo.io/ip -q -O -)"

# create system user for dante
useradd --shell /usr/sbin/nologin $username && echo "$username:$password" | chpasswd

# dante conf
cat <<EOT > /etc/danted.conf
logoutput: /var/log/socks.log
internal: eth0 port = 8080
external: eth0
method: username none
user.privileged: root
user.notprivileged: nobody
client pass {
from: 0.0.0.0/0 to: 0.0.0.0/0
log: error connect disconnect
}
client block {
from: 0.0.0.0/0 to: 0.0.0.0/0
log: connect error
}
pass {
from: 0.0.0.0/0 to: 0.0.0.0/0
log: error connect disconnect
}
block {
from: 0.0.0.0/0 to: 0.0.0.0/0
log: connect error
}


EOT
# And we have a little bit problem with this message from `systemctl status danted.service`
#               danted.service: Failed to read PID from file /var/run/danted.pid: Invalid argument
#systemctl restart danted.service
sudo service danted restart

#information
echo "--------------------------------------------------------------------------------------------------"
sudo service danted status
echo "--------------------------------------------------------------------------------------------------"
echo "--------------------------------------------------------------------------------------------------"
echo "Proxy IP: $external_ip"
echo "SOCKS5 port: 8080"
echo "Username: $username"
echo "Password: $password"
